const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

//------------------------------------------------------------------------------
// Question:-1  Get all items that are available

function checkingAvailble(items) {
  let presentItem = items.filter((index) => {
    return index.available == true;
  });
  console.log(presentItem);
}

checkingAvailble(items);

//----------------------------------------------------------------------------------

// Questions:- 2 Get all items containing only Vitamin C.

function onlyVitaminC(items) {
  let vitaminC = items.filter(function (index) {
    return index.contains.includes("Vitamin C");
  });
  console.log(vitaminC);
}
onlyVitaminC(items);

//-----------------------------------------------------------------------------
//Questions-3 Get all items containing Vitamin A.

function containVitaminA(items) {
  let filterVitaminA = items.filter(function (index) {
    return index.contains === "Vitamin A";
  });
  console.log(filterVitaminA);
}
containVitaminA(items);

//-----------------------------------------------------------------------------
// Question:-4. Group items based on the Vitamins that they contain in the following format:
//  {
//      "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

//   not completed .

//------------------------------------------------------------------------------------
// Questions:- 5  Sort items based on number of Vitamins they contain.

function numberOfVitaminContain(items) {
  let numbersOfVitamins = items.sort(function (vitamin1, vitamin2) {
    let countValue1 = vitamin1.contains.length;
    // console.log(countValue1);
    let countValue2 = vitamin2.contains.length;
    //  console.log(countValue2);
    if (countValue1 > countValue2) {
      return 1;
    } else {
      return -1;
    }
  });
  console.log(numbersOfVitamins);
}
numberOfVitaminContain(items);
