let person = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

//------------------------------------------------------------------
// Questions:-1 Find all people who are Agender

function findPeople(person) {
  let filte = person
    .filter(function (el) {
      return el.gender == "Agender";
    })
    .map(function (ele) {
      return ele.first_name + "" + ele.last_name;
    });
  console.log(filte);
}

//findPeople(person);

//-------------------------------------------------------------
// Question:-2 Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

function splitIPAddress(person) {
  let ip = [];
  let ipV4 = [];
  person.forEach(function (ele) {
    ip.push(ele.ip_address);
  });
  // console.log(ip);
  //   let split = [];
  for (let index = 0; index < ip.length; index++) {
    let el = ip[index];
    let spli = el.split(".");
    ipV4.push(spli);
  }

  //console.log(ipV4);
  let result = [];

  for (let index = 0; index < ipV4.length; index++) {
    let res = [];
    let temp = ipV4[index];
    for (let jindex = 0; jindex < temp.length; jindex++) {
      res.push(Number(temp[jindex]));
    }
    result.push(res);
  }
  console.log(result);
}
//splitIPAddress(person);

//=====================================================================

// Questions:- 3 Find the sum of all the second components of the ip addresses.

function findSumSecond(person) {
  let ip = [];
  let ipV4 = [];
  person.forEach(function (ele) {
    ip.push(ele.ip_address);
  });
  // console.log(ip);
  //   let split = [];
  for (let index = 0; index < ip.length; index++) {
    let el = ip[index];
    let spli = el.split(".");
    ipV4.push(spli);
  }
  // console.log(ipV4);
  let allSecondSum = 0;
  for (let index = 0; index < ipV4.length; index++) {
    let res = [];
    let temp = ipV4[index];
    for (let jindex = 0; jindex < temp.length - 2; jindex++) {
      // res.push(Number(temp[j]));
      allSecondSum += Number(temp[1]);
    }
  }
  console.log(allSecondSum);
}
//findSumSecond(person);

//===================================================================
//  3. Find the sum of all the fourth components of the ip addresses.
function findSumfourth(person) {
  let ip = [];
  let ipV4 = [];
  person.forEach(function (ele) {
    ip.push(ele.ip_address);
  });
  // console.log(ip);
  //   let split = [];
  for (let index = 0; index < ip.length; index++) {
    let el = ip[index];
    let spli = el.split(".");
    ipV4.push(spli);
  }
  // console.log(ipV4);
  let allSecondSum = 0;
  for (let index = 0; index < ipV4.length; index++) {
    let res = [];
    let temp = ipV4[index];
    for (let jindex = 0; jindex < temp.length; jindex++) {
      // res.push(Number(temp[j]));
      allSecondSum += Number(temp[3]);
    }
  }
  console.log(allSecondSum);
}
//findSumfourth(person);

//================================================================

// Quetions 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.

function fullName(person) {
  let full = person.map(function (ele) {
    return (ele.fullName = ele.first_name + " " + ele.last_name);
  });
  console.log(full);
}
// fullName(person);

//==============================================================
// Question 5. Filter out all the .org emails

function filterOut(person) {
  let dotORG = person.filter(function (ele) {
    return ele.email.endsWith(".org");
  });
  console.log(
    dotORG.map(function (e) {
      return e.email;
    })
  );
}
filterOut(person);

//=============================================================
// Questions 6. Calculate how many .org, .au, .com emails are there

//===================================================================
// 7. Sort the data in descending order of first name

function sortDescending(person) {
  let firstName = [];
  person.forEach(function (ele) {
    firstName.push(ele.first_name);
  });
  console.log(firstName.sort());
  let decending = [];
  for (let index = firstName.length - 1; index > 0; index--) {
    decending.push(firstName[index]);
  }
  console.log(decending);
}
//sortDescending(person);

//===============================================================
