const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

// Q1.  Find all users who are interested in playing video games.

function findUser(users) {
  let userArr1 = Object.entries(users);
  //  console.log(userArr1);
  return userArr1
    .map((index) => {
      return index[1];
      //   console.log(index[1]);
    })
    .filter((object) => {
      if (object.hasOwnProperty("interests")) {
        return (
          object.interests.toString().includes("Playing Video Games") ||
          object.interests.toString().includes("Video Games")
        );
      }
    });
}
console.log(findUser(users));

// Q2 Find all users staying in Germany.

function allUserStayingInGermany(users) {
  let userArray = Object.entries(users);
  // console.log(userArray);
  return userArray
    .map((userInfo) => {
      return userInfo[1];
    })
    .filter((filterNationality) => {
      return filterNationality.nationality == "Germany";
    });
}

console.log(allUserStayingInGermany(users));

//Q4 Find all users with masters Degree.

function allUserMasterDegree(users) {
  let userArr4 = Object.entries(users);
  return userArr4
    .map((userinformation) => {
      return userinformation[1];
    })
    .filter((filterQualification) => {
      return filterQualification.qualification == "Masters";
    });
}
console.log(allUserMasterDegree(users));
